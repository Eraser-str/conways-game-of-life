import random
import curses


class Universe:
    """By Klas are all methods that change the state of the universe"""

    def __init__(self, scr, char=ord('*')):
        """Initialization of the universe

        :param src: curses screen object to use for display
        :param char: character used to render live cells

        :type src: curses screen object
        :type char: integer representing the Unicode code point of the character

        """
        self.state = {}
        self.scr = scr
        Y, X = self.scr.getmaxyx()
        self.X, self.Y = X - 2, Y - 2 - 1
        self.char = char
        self.scr.clear()

        # Draw a border
        border_line = '+' + (self.X * '-') + '+'
        self.scr.addstr(0, 0, border_line)
        self.scr.addstr(self.Y + 1, 0, border_line)
        for y in range(0, self.Y):
            self.scr.addstr(1 + y, 0, '|')
            self.scr.addstr(1 + y, self.X + 1, '|')
        self.scr.refresh()

    def set(self, y, x):
        """Set a cell to the live state"""
        if x < 0 or self.X <= x or y < 0 or self.Y <= y:
            raise ValueError("Coordinates out of range %i,%i" % (y, x))
        self.state[x, y] = 1

    def toggle(self, y, x):
        """Toggle a cell's state between live and dead"""
        if x < 0 or self.X <= x or y < 0 or self.Y <= y:
            raise ValueError("Coordinates out of range %i,%i" % (y, x))
        if (x, y) in self.state:
            del self.state[x, y]
            self.scr.addch(y + 1, x + 1, ' ')
        else:
            self.state[x, y] = 1
            if curses.has_colors():
                # Choose a random color
                self.scr.attrset(curses.color_pair(random.randrange(1, 7)))
            self.scr.addch(y + 1, x + 1, self.char)
            self.scr.attrset(0)
        self.scr.refresh()

    def erase(self):
        """Clear the entire board and update the board display"""
        self.state = {}
        self.display(update_board=False)

    def display(self, update_board=True):
        """Display the whole board, optionally computing one generation"""
        M, N = self.X, self.Y
        if not update_board:
            for i in range(0, M):
                for j in range(0, N):
                    if (i, j) in self.state:
                        self.scr.addch(j + 1, i + 1, self.char)
                    else:
                        self.scr.addch(j + 1, i + 1, ' ')
            self.scr.refresh()
            return

        d = {}
        self.boring = 1
        for i in range(0, M):
            L = range(max(0, i - 1), min(M, i + 2))
            for j in range(0, N):
                s = 0
                live = (i, j) in self.state
                for k in range(max(0, j - 1), min(N, j + 2)):
                    for l in L:
                        if (l, k) in self.state:
                            s += 1
                s -= live
                if s == 3:
                    # Birth
                    d[i, j] = 1
                    if curses.has_colors():
                        #Let's pick a random color!
                        self.scr.attrset(curses.color_pair(random.randrange(1, 7)))
                    self.scr.addch(j + 1, i + 1, self.char)
                    self.scr.attrset(0)
                    if not live: self.boring = 0
                elif s == 2 and live:
                    d[i, j] = 1  # Survival
                elif live:
                    # Death
                    self.scr.addch(j + 1, i + 1, ' ')
                    self.boring = 0
        self.state = d
        self.scr.refresh()

    def make_random(self):
        """Randomly fill"""
        self.state = {}
        for i in range(0, self.X):
            for j in range(0, self.Y):
                if random.random() > 0.5:
                    self.set(j, i)

    def save_or_load(self, num, stdscr, menu_y):
        try:
            with open('maps/' + num + '.txt', 'r') as f:
                self.state = eval(f.read())
            self.display(update_board=False)
        except IOError:
            with open('maps/' + num + '.txt', 'w') as f:
                f.write(str(self.state))
            stdscr.addstr(menu_y, 6, 'Saved')


class Menu:

    def erase_menu(self, stdscr, menu_y):
        """Clear the space where the menu locate"""
        stdscr.move(menu_y, 0)
        stdscr.clrtoeol()
        stdscr.move(menu_y + 1, 0)
        stdscr.clrtoeol()

    def display_menu(self, stdscr, menu_y):
        """Display the menu"""
        self.erase_menu(stdscr, menu_y)

        stdscr.addstr(menu_y + 1, 2,
                      'Use the cursor keys to move, and space or Enter to toggle a cell.')
        stdscr.addstr(menu_y + 2, 2,
                      'E)rase the board, R)andom fill, N)ext step once or C)ontinuously, Q)uit')
        stdscr.addstr(menu_y + 3, 2,
                      'num 0-9 save universe or load if universe already exist')
        stdscr.attrset(0)


def main_loop(stdscr):
    """The main loop, which handles all movements and keystrokes events"""

    # Get the necessary values and create the necessary instances
    stdscr.clear()
    stdscr_y, stdscr_x = stdscr.getmaxyx()
    subwin = stdscr.subwin(stdscr_y - 3, stdscr_x, 0, 0)

    board = Universe(subwin)
    board.display(update_board=False)

    menu = Menu()

    menu_y = (stdscr_y - 3) - 1
    menu.display_menu(stdscr, menu_y)

    # Initialization the color pairs
    if curses.has_colors():
        curses.init_pair(1, curses.COLOR_BLUE, 0)
        curses.init_pair(2, curses.COLOR_CYAN, 0)
        curses.init_pair(3, curses.COLOR_GREEN, 0)
        curses.init_pair(4, curses.COLOR_MAGENTA, 0)
        curses.init_pair(5, curses.COLOR_RED, 0)
        curses.init_pair(6, curses.COLOR_YELLOW, 0)
        curses.init_pair(7, curses.COLOR_WHITE, 0)

    # Set up the mask to listen for mouse events
    curses.mousemask(curses.BUTTON1_CLICKED)

    # xpos, ypos are the cursor's position
    xpos, ypos = board.X // 2, board.Y // 2

    # Main loop:
    while True:
        stdscr.move(1 + ypos, 1 + xpos)  # Move the cursor
        c = stdscr.getch()  # Get a keystroke

        if 0 < c < 256:
            c = chr(c)
            if c in ' \n':
                board.toggle(ypos, xpos)
            elif c in 'Cc':
                menu.erase_menu(stdscr, menu_y)
                stdscr.addstr(menu_y, 6, ' Hit any key to stop continuously '
                                         'updating the screen.')
                stdscr.refresh()
                # Activate no delay mode; getch() will return -1
                # if no keystroke is available, instead of waiting.
                stdscr.nodelay(1)
                while True:
                    c = stdscr.getch()
                    if c != -1:
                        break
                    stdscr.addstr(0, 0, '/')
                    stdscr.refresh()
                    board.display()
                    stdscr.addstr(0, 0, '+')
                    stdscr.refresh()

                stdscr.nodelay(0)  # Disable no delay mode
                menu.display_menu(stdscr, menu_y)

            elif c in 'Ee':
                board.erase()
            elif c in 'Nn':
                board.display()
            elif c in 'Qq':
                break
            elif c in 'Rr':
                board.make_random()
                board.display(update_board=False)
            elif c in '0123456789':
                board.save_or_load(c, stdscr, menu_y)
            else:
                pass  # Ignore incorrect keys
        elif c == curses.KEY_UP and ypos > 0:
            ypos -= 1
        elif c == curses.KEY_DOWN and ypos < board.Y - 1:
            ypos += 1
        elif c == curses.KEY_LEFT and xpos > 0:
            xpos -= 1
        elif c == curses.KEY_RIGHT and xpos < board.X - 1:
            xpos += 1
        elif c == curses.KEY_MOUSE:
            (mouse_id, mouse_x, mouse_y, mouse_z, button_state) = curses.getmouse()
            if (mouse_x > 0 and mouse_x < board.X + 1) and (mouse_y > 0 and mouse_y < board.Y + 1):
                xpos = mouse_x - 1
                ypos = mouse_y - 1
                board.toggle(ypos, xpos)
            else:
                # They've clicked outside the board
                curses.flash()
        else:
            # Ignore incorrect keys
            pass


def main(stdscr):
    main_loop(stdscr)


if __name__ == '__main__':
    curses.wrapper(main)